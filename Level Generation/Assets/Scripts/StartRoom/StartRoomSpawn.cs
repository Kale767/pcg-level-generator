﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartRoomSpawn : MonoBehaviour {

    //ArrayList<GameObject> NPCCharacters;

    //public List <GameObject> NPCCharacters = new List<GameObject>();

    public GameObject[] NPCCharacters;

    public static StartRoomSpawn startRoomSpawn;
    public Transform[] NPCCharacterSpawnpoints;

    // public GameObject[] NPCCharacters;

    public Transform[] environmentPropsSpawnpoints;

    public GameObject[] environmentProps;

    public float obstacleCheckRadius = 3f;

    public int maxSpawnAttempts = 5;

    void Start () {

        SpawnCharacter ();
    }

    public void SpawnCharacter () {

        int spawnNPCCharacterIndex = Random.Range (0, NPCCharacters.Length);

        int NPCCharacterObjectIndex = Random.Range (0, NPCCharacters.Length);

        Instantiate (NPCCharacters[NPCCharacterObjectIndex], NPCCharacterSpawnpoints[spawnNPCCharacterIndex].position, NPCCharacterSpawnpoints[spawnNPCCharacterIndex].rotation);

        //Environment Spawns

        //Instantiate (environmentProps[environmentPropsObjectIndex], environmentPropsSpawnpoints[spawnEnvironmentPropsIndex].position, environmentPropsSpawnpoints[spawnEnvironmentPropsIndex].rotation);

        for (int i = 0; i < environmentProps.Length; i++) {

            int spawnEnvironmentPropsIndex = Random.Range (0, environmentProps.Length);

            int environmentPropsObjectIndex = Random.Range (0, environmentProps.Length);

            Instantiate (environmentProps[environmentPropsObjectIndex], environmentPropsSpawnpoints[spawnEnvironmentPropsIndex].position, environmentPropsSpawnpoints[spawnEnvironmentPropsIndex].rotation);

        }

    }

    

}