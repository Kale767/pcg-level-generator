﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnItems : MonoBehaviour {
    public Transform[] spawnPoints;

    public GameObject[] enemies;

    public GameObject[] environmentProps;

    public GameObject[] bosses;

    public GameObject[] npcCharacters;

    void Start () {

        //InvokeRepeating ("SpawnItem",spawnTimer,spawnTimer); 
        //using string to repeat the fucntions name in the brackets

        SpawnItem ();
    }

    void SpawnItem () {

        int spawnEnemyIndex = Random.Range (0, enemies.Length);
        int spawnBossIndex = Random.Range (0, bosses.Length);
        int spawnEnvironmentPropsIndex = Random.Range (0, environmentProps.Length);
        int spawnNPCCharacterIndex = Random.Range (0, npcCharacters.Length);

        int bossObjectIndex = Random.Range (0, bosses.Length);
        int enemyObjectIndex = Random.Range (0, enemies.Length);
        int environmentPropsObjectIndex = Random.Range (0, environmentProps.Length);
        int NPCCharacterObjectIndex = Random.Range (0, npcCharacters.Length);

        Instantiate (bosses[bossObjectIndex], spawnPoints[spawnBossIndex].position, spawnPoints[spawnBossIndex].rotation);
        Instantiate (enemies[enemyObjectIndex], spawnPoints[spawnEnemyIndex].position, spawnPoints[spawnEnemyIndex].rotation);
        Instantiate (environmentProps[environmentPropsObjectIndex], spawnPoints[spawnEnvironmentPropsIndex].position, spawnPoints[spawnEnvironmentPropsIndex].rotation);
        Instantiate (npcCharacters[NPCCharacterObjectIndex], spawnPoints[spawnNPCCharacterIndex].position, spawnPoints[spawnNPCCharacterIndex].rotation);

    }
}