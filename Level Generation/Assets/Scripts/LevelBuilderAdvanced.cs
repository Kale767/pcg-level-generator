﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProbabilityValue {

    // public LevelGenerate levelGenerate;

    public float probability;

    public float probabilityIncrement = 1f;

    [Header ("Debug")]
    public float probabilityLikelinessOffset;
    public int rollCount;
}

public enum SpawnVariation {

    Low,
    Medium,
    High
}

public class LevelBuilderAdvanced : MonoBehaviour {

    [SerializeField] List<ProbabilityValue> probabilityValues = new List<ProbabilityValue> ();
    [Header ("Debug")]
    [SerializeField] float totalProbability = 100;

    public static LevelBuilderAdvanced levelBuilderAdvanced;

    public Room startRoomPrefab, endRoomPrefab;

    //public PlayerControlelr playerPrefab;

    public List<Room> roomPrefabs = new List<Room> ();

    public Vector2 iterationRange = new Vector2 (3, 10);

    List<Doorway> availableDoorways = new List<Doorway> ();

    StartRoom startRoom;

    EndRoom endRoom;

    List<Room> placedRoom = new List<Room> ();

    LayerMask roomLayerMask;

    //  PlayerControlelr player;

    void Start () {

        // roomLayerMask = LayerMask.GetMask ("Room");
        //Second Vid

        //StartCoroutine (GenerateLevel ());
    }

  

    public void GenerateButton () {

        StartCoroutine (GenerateLevel ());

    }

    public void DeleteGeneration () {

        ResetLevelGenerator ();

        //StartRoomSpawn.startRoomSpawn.DeleteEverything ();
    }

    public IEnumerator GenerateLevel () {

        WaitForSeconds startup = new WaitForSeconds (1);
        WaitForFixedUpdate interval = new WaitForFixedUpdate ();
        yield return startup;

        //Place the Start Room

        PlaceStartRoom ();
        yield return interval;

        //Random Iterations

        int iterations = Random.Range ((int) iterationRange.x, (int) iterationRange.y);

        for (int i = 0; i < iterations; i++) {

            //Place random room from List
            PlaceRoom ();
            yield return interval;
        }

        //Place end Room
        PlaceEndRoom ();
        yield return interval;

        Debug.Log ("Level Generation Finished");

        //ResetLevelGenerator ();
    }

    void PlaceStartRoom () {

        //Debug.Log ("Place Start Room");
        startRoom = Instantiate (startRoomPrefab) as StartRoom;
        startRoom.transform.parent = this.transform;

        //Get all doorways from room and add to list
        AddDoorwaysToList (startRoom, ref availableDoorways);

        //Position Room
        startRoom.transform.position = Vector3.zero;
        startRoom.transform.rotation = Quaternion.identity;
        //Placing it at the origin of the world

    }
    void AddDoorwaysToList (Room room, ref List<Doorway> list) {

        foreach (Doorway doorway in room.doorWays) {

            int r = Random.Range (0, list.Count);
            list.Insert (r, doorway);

        }

    }

    void PlaceRoom () {

        //Debug.Log ("Place Random Room From List");

        Room currentRoom = Instantiate (roomPrefabs[Random.Range (0, roomPrefabs.Count)]) as Room;
        currentRoom.transform.parent = this.transform;

        //Creating the doorway list

        List<Doorway> allAvailableDoorways = new List<Doorway> (availableDoorways);
        List<Doorway> currentRoomDoorways = new List<Doorway> ();
        AddDoorwaysToList (currentRoom, ref currentRoomDoorways);

        //Getting the doorways from the current room
        AddDoorwaysToList (currentRoom, ref availableDoorways);

        bool roomPlaced = false;

        //Test all avalable Quads (Doorways)

        foreach (Doorway availableDoorway in allAvailableDoorways) {

            foreach (Doorway currentDoorway in currentRoomDoorways) {

                PositionRoomAtDoorway (ref currentRoom, currentDoorway, availableDoorway);

                //Check that Overlap stuff
                if (CheckRoomOverlap (currentRoom)) {

                    continue;

                }

                roomPlaced = true;

                //Add room to list (Cleanup purposes)
                placedRoom.Add (currentRoom);

                //Remove occupied doorways
                currentDoorway.gameObject.SetActive (false);
                availableDoorways.Remove (currentDoorway);

                availableDoorway.gameObject.SetActive (false);
                availableDoorways.Remove (availableDoorway);

                //Leave Loop
                break;

            }

            if (roomPlaced) {

                break;

            }

        }

        //Room cannot be placed
        if (!roomPlaced) {

            Destroy (currentRoom.gameObject);
            ResetLevelGenerator ();

        }

    }

    void PlaceEndRoom () {

        endRoom = Instantiate (endRoomPrefab) as EndRoom;
        endRoom.transform.parent = this.transform;

        List<Doorway> allAvailableDoorways = new List<Doorway> (availableDoorways);
        Doorway doorway = endRoom.doorWays[0];

        bool roomPlaced = false;

        foreach (Doorway availableDoorway in allAvailableDoorways) {

            Room room = (Room) endRoom;

            PositionRoomAtDoorway (ref room, doorway, availableDoorway);

            //Check that Overlap stuff
            if (CheckRoomOverlap (endRoom)) {

                continue;

            }

            roomPlaced = true;

            //Remove occupied doorways
            doorway.gameObject.SetActive (false);
            availableDoorways.Remove (doorway);

            availableDoorway.gameObject.SetActive (false);
            availableDoorways.Remove (availableDoorway);

            //Leave Loop
            break;

        }

        //Room cannot be placed
        if (!roomPlaced) {

            ResetLevelGenerator ();

        }

    }

    void ResetLevelGenerator () {

        // Debug.LogError ("Reset Level Generator");
        // StopCoroutine (GenerateLevel ());

        //Delete Rooms

        if (startRoom) {

            Destroy (startRoom.gameObject);
        }

        if (endRoom) {

            Destroy (endRoom.gameObject);
        }

        foreach (Room room in placedRoom) {

            Destroy (room.gameObject);
        }

        //Clear Lists

        placedRoom.Clear ();
        availableDoorways.Clear ();

        //Reset Coroutine

        //StartCoroutine (GenerateLevel ());

    }

    void PositionRoomAtDoorway (ref Room room, Doorway roomDoorway, Doorway targetDoorway) {

        //Reset room position and rotation
        room.transform.position = Vector3.zero;
        room.transform.rotation = Quaternion.identity;

        //Rotate Room to match orientation
        Vector3 targetDoorwayEuler = targetDoorway.transform.eulerAngles;
        Vector3 roomDoorwayEuler = roomDoorway.transform.eulerAngles;
        float deltaAngle = Mathf.DeltaAngle (roomDoorwayEuler.y, targetDoorwayEuler.y);
        Quaternion currentRoomTargetRotation = Quaternion.AngleAxis (deltaAngle, Vector3.up);
        room.transform.rotation = currentRoomTargetRotation * Quaternion.Euler (0, 180f, 0);

        //Position Room
        Vector3 roomPositionOffset = roomDoorway.transform.position - room.transform.position;
        room.transform.position = targetDoorway.transform.position - roomPositionOffset;

    }

    [ContextMenu ("Roll Random")]

    public void RollNumber () {

        totalProbability = 0;
        probabilityValues.ForEach (x => totalProbability += x.probability + x.probabilityLikelinessOffset); //Add all probabilities together

        float randomNum = Random.Range (0, totalProbability);
        bool foundCandidate = false;
        ProbabilityValue selectedProbabilityValue = null;
        float runningTotal = 0;

        for (int i = 0; i < probabilityValues.Count; i++) {
            bool offsetValue = true;
            if (!foundCandidate) {
                //Searching for candidate
                float probabilityNumber = (probabilityValues[i].probability + probabilityValues[i].probabilityLikelinessOffset);
                runningTotal += probabilityNumber;

                if (randomNum <= runningTotal) {
                    selectedProbabilityValue = probabilityValues[i];
                    foundCandidate = true;
                    offsetValue = false;

                    probabilityValues[i].probabilityLikelinessOffset = 0; //Reset offset value
                    probabilityValues[i].rollCount++; //FOR DEBUG PURPOSES

                    // Debug.Log ($"Rolled {randomNum} / {runningTotal} and found {selectedProbabilityValue.enemyDifficulty} enemy difficulty");
                }
            }

            if (offsetValue) {
                //Adjusting offset for others
                probabilityValues[i].probabilityLikelinessOffset += probabilityValues[i].probabilityIncrement;
            }
        }

    }

    bool CheckRoomOverlap (Room room) {

        Bounds bounds = room.RoomBounds;
        bounds.Expand (-0.1f);
        Collider[] colliders = Physics.OverlapBox (bounds.center, bounds.size / 2, room.transform.rotation, roomLayerMask);

        if (colliders.Length > 0) {
            //Ignore collisions with current room
            foreach (Collider c in colliders) {

                if (c.transform.parent.gameObject.Equals (room.gameObject)) {

                    continue;

                } else {

                    Debug.LogError ("Overlap Detected");

                    return true;
                }

            }

        }

        return false;

    }

}