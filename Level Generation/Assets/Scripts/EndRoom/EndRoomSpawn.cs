﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndRoomSpawn : MonoBehaviour {
    public Transform[] environmentPropsSpawnpoints;

    public GameObject[] environmentProps;

    public Transform[] bossesSpawnPoints;
    public GameObject[] bosses;

    void Start () {

        SpawnStuff ();
    }

    void SpawnStuff () {

        int spawnBossIndex = Random.Range (0, bosses.Length);

        int bossObjectIndex = Random.Range (0, bosses.Length);

        Instantiate (bosses[bossObjectIndex], bossesSpawnPoints[spawnBossIndex].position, bossesSpawnPoints[spawnBossIndex].rotation);

        //Environmental Spawns

        for (int i = 0; i < environmentProps.Length; i++) {

            int spawnEnvironmentPropsIndex = Random.Range (0, environmentProps.Length);

            int environmentPropsObjectIndex = Random.Range (0, environmentProps.Length);

            Instantiate (environmentProps[environmentPropsObjectIndex], environmentPropsSpawnpoints[spawnEnvironmentPropsIndex].position, environmentPropsSpawnpoints[spawnEnvironmentPropsIndex].rotation);

        }
    }

}