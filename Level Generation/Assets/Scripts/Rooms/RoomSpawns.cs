﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomSpawns : MonoBehaviour {
    public Transform[] environmentPropsSpawnpoints;

    public GameObject[] environmentProps;

    public Transform[] enemySpawnPoints;

    public GameObject[] enemies;

    void Start () {

        SpawnEverything ();
    }

    void SpawnEverything () {

        //Enemy Spawns

        int spawnEnemiesIndex = Random.Range (0, enemies.Length);

        int enemiesObjectIndex = Random.Range (0, enemies.Length);

        Instantiate (enemies[enemiesObjectIndex], enemySpawnPoints[spawnEnemiesIndex].position, enemySpawnPoints[spawnEnemiesIndex].rotation);

        //Environmental Spawns

        for (int i = 0; i < environmentProps.Length; i++) {
            
            int spawnEnvironmentPropsIndex = Random.Range (0, environmentProps.Length);

            int environmentPropsObjectIndex = Random.Range (0, environmentProps.Length);

            Instantiate (environmentProps[environmentPropsObjectIndex], environmentPropsSpawnpoints[spawnEnvironmentPropsIndex].position, environmentPropsSpawnpoints[spawnEnvironmentPropsIndex].rotation);

        }
    }
}