﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.SceneManagement;

public class SmallController : MonoBehaviour {
    //slider bar

    public float speed = 10;
    public float rotationSpeed = 80;
  //  public float grav = 8;

    //public float runSpeed = 10f;

  //  public float backSpeed = 2;

    float rotation = 4f;

    Vector3 moveDir = Vector3.zero;

    //CharacterController cC;

    void Start () {

        //cC = GetComponent<CharacterController> ();

    }

    void Update () {

         if (Input.GetKeyDown (KeyCode.W)) {
            
            moveDir = new Vector3 (0, 1, 1);
            moveDir = moveDir * speed;

            Debug.Log("W is Pressed");

        }

        if (Input.GetKeyUp (KeyCode.W)) {
            moveDir = new Vector3 (0, 0, 0);

        }

         if (Input.GetKeyDown (KeyCode.S)) {
            moveDir = new Vector3 (0, 0, -1);
            moveDir = moveDir * speed;
            //Debug.Log ("Is moving forward");

        }

        if (Input.GetKeyUp (KeyCode.S)) {
            moveDir = new Vector3 (0, 0, 0);

        }

        //Direction Change
        rotation += Input.GetAxis ("Horizontal") * rotationSpeed * Time.deltaTime;
        transform.eulerAngles = new Vector3 (0, rotation, 0);

       // Movement ();
        //BackMovement ();
        //SpeedRun ();

    }

    //Alter for the enemy to do damage

    public void Movement () {

        if (Input.GetKeyDown (KeyCode.W)) {
            
            moveDir = new Vector3 (0, 1, 1);
            moveDir = moveDir * speed;

            Debug.Log("W is Pressed");

        }

        if (Input.GetKeyUp (KeyCode.W)) {
            moveDir = new Vector3 (0, 0, 0);

        }

        //Direction Change
        rotation += Input.GetAxis ("Horizontal") * rotationSpeed * Time.deltaTime;
        transform.eulerAngles = new Vector3 (0, rotation, 0);

    }

    void BackMovement () {

        if (Input.GetKeyDown (KeyCode.S)) {
            moveDir = new Vector3 (0, 0, -1);
            moveDir = moveDir * speed;
            //Debug.Log ("Is moving forward");

        }

        if (Input.GetKeyUp (KeyCode.S)) {
            moveDir = new Vector3 (0, 0, 0);

        }

    }

}