
# PCG Cave Generator

A Cave Generator designed for Unity

It allows the user to create a procedurally generated cave system from prefabs using quads.

# Purpose

The purpose of this weather system is to offer users a way to create a cave system from the created prefabs or their own created prefabs using a collection of quads and individual room scripts.

# Usage

A Level Generator Manager is needed that houses the room prefabs, the start and the end. A canvas with two buttons is needed to generate the system and another to delete the generation. This can be altered in the code using a Start function to generate as a scene is loaded. 

Each room prefab must use quads to act as the doorways, which will in turn connect other instantiated objects to the rooms.

The scripts that are needed:

Doorway
EndRoom
LevelBuilder
Room
StartRoom
EndRoomSpawn
RoomSpawns
StartRoomSpawns

**To use the full scene**

In the scenes folder, open the 'LevelGenerator' file.

**What can be controlled?**

Once the scripts have been applied or copied, the user can add prefabs to the LevelBuilder script and increase the amount of prefabs that can be used. The iterations on the X and Y can be altered to increase the amount of rooms that spawn until the end room is finally placed.

# Environment

The game objects used:

Cave prefabs were created using Blender
Models (characters) downloaded from Mixamo.com

# Tutorials Used

By user OctoMan on YouTube:

https://www.youtube.com/watch?v=iLTP4EbM1N4

Used for instantiating objects at random positions.
